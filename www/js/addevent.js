function globalVars(){
  
}

//Functions to invoke calendar events
function add_event(){
  var startDate = new Date("July 24, 2014 13:00:00");
  var endDate   = new Date("July 24, 2014 14:30:00");
  var title     = "Your Title Here";
  var location  = "Office";
  var notes     = "Need to do a meeting";
  var success   = function(message) { alert("Success: " + JSON.stringify(message)); };
  var error     = function(message) { alert("Error: " + message); };
  var cal       = window.plugins.calendar;
  cal.createEvent(title,location,notes,startDate,endDate,success,error);
}

function delete_event(){
  var startDate = new Date("July 24, 2014 13:00:00");
  var endDate   = new Date("July 24, 2014 14:30:00");
  var newTitle  = "New Title";
  var location  = "Office";
  var notes     = "Need to do a meeting";
  var success   = function(message) { alert("Success: " + JSON.stringify(message)); };
  var error     = function(message) { alert("Error: " + message); };
  var cal       = window.plugins.calendar;
  cal.deleteEvent(newTitle,location,notes,startDate,endDate,success,error);
}

function update_event(){
  var startDate = new Date("July 24, 2014 13:00:00");
  var endDate   = new Date("July 24, 2014 14:30:00");
  var title  = "Your Title";
  var newTitle  = "New Title";
  var location  = "Office";
  var notes     = "Need to do a meeting";
  var success   = function(message) { alert("Success: " + JSON.stringify(message)); };
  var error     = function(message) { alert("Error: " + message); };
  var cal       = window.plugins.calendar;
  indow.plugins.calendar.modifyEvent(title,location,notes,startDate,endDate,newTitle,location,notes,startDate,endDate,success,error);
}


function interactive_event(){
  var startDate = new Date("July 24, 2014 13:00:00");
  var endDate   = new Date("July 24, 2014 14:30:00");
  var title     = "Matern App Title Here";
  var location  = "Home";
  var notes     = "Need to add an event";
  var success   = function(message) { alert("Success: " + JSON.stringify(message)); };
  var error     = function(message) { alert("Error: " + message); };
  var cal       = window.plugins.calendar;
  cal.createEventInteractively(title,location,notes,startDate,endDate,success,error);
}

//iOS only
function create_cal(){
  var cal       = window.plugins.calendar;
  var createCalOptions = cal.getCalendarOptions();
  createCalOptions.calendarName = "My Cal Name";
  createCalOptions.calendarColor = "#FF0000"; // an optional hex color (with the # char), default is null, so the OS picks a color
  var success   = function(message) { alert("Success: " + JSON.stringify(message)); };
  var error     = function(message) { alert("Error: " + message); };
  cal.createCalendar(createCalOptions,success,error);
}

function namedCal_event(){
  var cal       = window.plugins.calendar;
  var calOptions = cal.getCalendarOptions();
  var calendarName = "MaternAppCal"; 
  var startDate = new Date("July 24, 2014 13:00:00");
  var endDate   = new Date("July 24, 2014 14:30:00");
  var title     = "Your Title Here";
  var location  = "Office";
  var notes     = "Need to do a meeting";
  var success   = function(message) { alert("Success: " + JSON.stringify(message)); };
  var error     = function(message) { alert("Error: " + message); };
  cal.createEventInNamedCalendar(title,location,notes,startDate,endDate,calendarName,success,error);
}

function list_cal(){
  var cal       = window.plugins.calendar;
  var success   = function(message) { alert("Success: " + JSON.stringify(message)); };
  var error     = function(message) { alert("Error: " + message); };
  cal.listCalendars(success,error);
}

function find_event(){
  var startDate = new Date("July 24, 2014 13:00:00");
  var endDate   = new Date("July 24, 2014 14:30:00");
  var title     = "Your Title Here";
  var location  = "Office";
  var notes     = "Need to do a meeting";
  var success   = function(message) { alert("Success: " + JSON.stringify(message)); };
  var error     = function(message) { alert("Error: " + message); };
  var cal       = window.plugins.calendar;
  cal.findEvent(title,location,notes,startDate,endDate,success,error);
}



//Clickable Events
function addEvent(){document.addEventListener("deviceready", add_event, false);}
function addInteractiveEvent(){document.addEventListener("deviceready", interactive_event, false);}
function createCal(){document.addEventListener("deviceready", create_cal, false);}
function addNamedCalEvent(){document.addEventListener("deviceready", namedCal_event, false);}
function listCal(){document.addEventListener("deviceready", list_cal, false);}
function findEvent(){document.addEventListener("deviceready", find_event, false);}






//****############################################################Codes to invoke Calendar Evcents****############################################################//
 // var startDate = new Date(2014,7,20,18,0,0,0,0); // beware: month 0 = january, 11 = december
   // var endDate = new Date(2014,7,20,18,0,0,0,0);
    //var startDate = new Date("July 24, 2014 13:00:00");
    //var endDate   = new Date("July 24, 2014 14:30:00");
    //var title     = "New Title Here";
    //var newTitle  = "New Title";
   // var location  = "Office";
   // var notes     = "Need to do a meeting";
   // var success   = function(message) { alert("Success: " + JSON.stringify(message)); };
   // var error     = function(message) { alert("Error: " + message); };
   // var cal       = window.plugins.calendar;
   // cal.createEvent(title,location,notes,startDate,endDate,success,error);

  //window.plugins.calendar.modifyEvent(title,location,notes,startDate,endDate,newTitle,location,notes,startDate,endDate,success,error);

  //  window.plugins.calendar.deleteEvent(newTitle,location,notes,startDate,endDate,success,error);
    // window.plugins.calendar.listEventsInRange(startDate,endDate,success,error);
    
   // cal.createCalendar(calendarName,success,error);
   // window.plugins.calendar.createEvent(title,location,notes,startDate,endDate,success,error);
    //cal.createEvent(title,location,notes,startDate,endDate,success,error);









    // create a calendar (iOS only for now)
   //cal.createCalendar(calendarName,success,error);
    // if you want to create a calendar with a specific color, pass in a JS object like this:
   // var createCalOptions = cal.getCreateCalendarOptions();
  //  createCalOptions.calendarName = "My Cal Name";
   // createCalOptions.calendarColor = "#FF0000"; // an optional hex color (with the # char), default is null, so the OS picks a color
   // cal.createCalendar(createCalOptions,success,error);
    

    
    // delete a calendar (iOS only for now)
   // cal.deleteCalendar(calendarName,success,error);
    
    // create an event silently (on Android < 4 an interactive dialog is shown)
   // cal.createEvent(title,location,notes,startDate,endDate,success,error);
    

   
    // create an event silently (on Android < 4 an interactive dialog is shown which doesn't use this options) with options:
    //var calOptions = cal.getCalendarOptions(); // grab the defaults
   // calOptions.firstReminderMinutes = 120; // default is 60, pass in null for no reminder (alarm)
    //calOptions.secondReminderMinutes = 5;
    
    // Added these options in version 4.2.4:
    //calOptions.recurrence = "monthly"; // supported are: daily, weekly, monthly, yearly
    //calOptions.recurrenceEndDate = new Date(2015,6,1,0,0,0,0,0); // leave null to add events into infinity and beyond
    //calOptions.calendarName = "MyCreatedCalendar"; // iOS only
   // cal.createEventWithOptions(title,location,notes,startDate,endDate,calOptions,success,error);
    
    // create an event interactively (only supported on Android)
    //cal.createEventInteractively(title,location,notes,startDate,endDate,success,error);
     
    // create an event in a named calendar (iOS only for now)
    //cal.createEventInNamedCalendar(title,location,notes,startDate,endDate,calendarName,success,error);
    

    
    // find events
    //cal.findEvent(title,location,notes,startDate,endDate,success,error);
    
    // list all events in a date range (only supported on Android for now)
  //  cal.listEventsInRange(startDate,endDate,success,error);
    
    // list all calendar names - returns this JS Object to the success callback: [{"id":"1", "name":"first"}, ..]
  //  cal.listCalendars(success,error);
    
    // find all events in a named calendar (iOS only for now)
   // cal.findAllEventsInNamedCalendar(calendarName,success,error);
  

    /*
    // change an event (iOS only for now)
   var newTitle = "New title!";
   window.plugins.calendar.modifyEvent(title,location,notes,startDate,endDate,newTitle,location,notes,startDate,endDate,success,error);
    
    // delete an event (you can pass nulls for irrelevant parameters, note that on Android `notes` is ignored)
    window.plugins.calendar.deleteEvent(newTitle,location,notes,startDate,endDate,success,error);
    */