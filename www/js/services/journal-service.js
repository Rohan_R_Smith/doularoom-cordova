angular.module('doularoom.services.journal', [])

.factory('JournalService', function($rootScope, $q, CLASS_NAMES) {

  return {

    getJournalData: function(patientID, startNum, endNum){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      ibmData.Query.ofType(CLASS_NAMES.PREGNANCY_JOURNAL)
      .find({ PATIENTID: patientID})
      .then (function (_result) {

        //console.debug(_result);
        var _data = [];
        _result.forEach(function(obj){ _data.push(obj.attributes); });
        _data = _.sortBy(_data, function(obj) { return obj.TIME_RECORDED; });
        _data = _data.reverse();
        _data = _data.slice(startNum, endNum);

        return defer.resolve(_data);

      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    },

    createJournal: function(patientID, Journal){
      var defer = $q.defer();
      var ibmData = IBMData.getService();

      ibmData.Query.ofType(CLASS_NAMES.PREGNANCY_JOURNAL)
      .find()
      .then(function (_result) {

        _result = _.filter(_result, function(obj){
          return obj.attributes.hasOwnProperty('PJID');
        });

        _result = _result.sort(function(a, b){
          return a.attributes.PJID - b.attributes.PJID;
        });

        var returnedObj = {};
        returnedObj.latestPJID = _result[_result.length-1].attributes.PJID;
        console.log("_result: " + JSON.stringify(_result[_result.length-1].attributes));

        return returnedObj;

      }).then(function(_result){

        var newJournal = {
          "PATIENTID": patientID,
          "TIME_RECORDED": (new Date()).toISOString(),
          "PHOTO": ""
        };

        var returnedObj = _result;
        var newPJID = ++returnedObj.latestPJID;
        var journal = ibmData.Object.ofType(CLASS_NAMES.PREGNANCY_JOURNAL, newJournal);

        journal.set({"PJID": newPJID, "JSUMMARY": Journal.JSUMMARY});
        return journal.save().then(function(savedJournal){
          returnedObj.savedJournal = savedJournal.attributes;
          return defer.resolve(returnedObj);
        });

      },function(err){
        console.log(err);
        defer.reject(err);
      });

      return defer.promise;
    }

  };

});
