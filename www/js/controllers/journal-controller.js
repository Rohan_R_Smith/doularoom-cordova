angular.module('doularoom.controllers.journal', [])

.controller('JournalCtrl', function($rootScope, $scope, $window, InitBluemix, JournalService, PATIENT) {

    $scope.Journal = {};

  $scope.shareItem = function (a, b, c, d) {
      window.plugins.socialsharing.share(a, b, c, d);
  };

  $scope.refresh = function() {
    // Refresh
    if (!$scope.$$phase) {
        $scope.$apply();
    }
  };

  $scope.createJournal= function() {

      JournalService.createJournal(PATIENT.PATIENTID, $scope.Journal)
      .then(function(savedJournal) {
      },function(err){
        IBMBluemix.getLogger().warn(err);
      });

      $window.location.href = '#/app/journals';
      $scope.Journal = {};

  };

  $scope.loadJournal= function() {

      JournalService.getJournalData(PATIENT.PATIENTID, 0, 10)
      .then(function(journalData) {
        $scope.journalData = journalData;
        $scope.refresh();
      },function(err){
        IBMBluemix.getLogger().warn(err);
      });

  }

  if (!$rootScope.IBMBluemix) {
      InitBluemix.init().then(function() {
          $rootScope.IBMBluemix = IBMBluemix;
          $scope.loadJournal();
      });
  } else {
        // load a refresh from the cloud
        $scope.loadJournal();
  }

});
